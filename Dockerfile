FROM node:lts-alpine AS build

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci --silent
COPY . .
RUN npm run build

FROM nginx:1.19-alpine

ARG BUILD_DATE
ARG COMMIT
ARG VERSION

LABEL org.opencontainers.image.created=$BUILD_DATE \
      org.opencontainers.image.title="MyAccount" \
      org.opencontainers.image.description="Web interface to let users manage their account" \
      org.opencontainers.image.url="https://myaccount.tenoco.net" \
      org.opencontainers.image.source="https://gitlab.com/les-connecteurs/api/myaccount" \
      org.opencontainers.image.revision=$COMMIT \
      org.opencontainers.image.vendor="Les Connecteurs" \
      org.opencontainers.image.version=$VERSION

EXPOSE 80

ENV API_BASE_URL=https://api.localhost/
ENV AUTHORITY=https://accounts.localhost
ENV CLIENT_ID=00000000-0000-0000-0000-000000000000
ENV VERSION=${VERSION}
ENV COMMIT=${COMMIT}

COPY --from=build /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

COPY ./template.sh /docker-entrypoint.d/30-template.sh
