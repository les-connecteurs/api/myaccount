import { SavedPassword, GeneratedPassword } from "../../api/models";

export type State = {
  passwords?: SavedPassword[];
  generated?: GeneratedPassword;
  loading: boolean;
  error?: string;
};
