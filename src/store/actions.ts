import { User } from "oidc-client";
import { Action as OidcAction } from "redux-oidc";

import { Action as AppPasswordsAction } from "./apppasswords/actions";

export type Action = OidcAction<User | Error> | AppPasswordsAction;
