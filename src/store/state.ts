import { UserState } from "redux-oidc";
import { RouterState } from "connected-react-router";
import { LocationState } from "history";

import { State as AppPasswordsState } from "./apppasswords/state";

export type State<RS = LocationState> = {
  auth: UserState;
  router: RouterState<RS>;
  apppasswords: AppPasswordsState;
};
