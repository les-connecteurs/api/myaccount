import React from "react";

const BlockLinkLabel: React.FC = (props) => {
  return <div className="w-32 uppercase text-xs">{props.children}</div>;
};

export default BlockLinkLabel;
