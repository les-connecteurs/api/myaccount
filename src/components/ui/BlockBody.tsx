import React from "react";

const BlockBody: React.FC = (props) => {
  return (
    <div className="text-gray-700 text-base p-4 pt-0">{props.children}</div>
  );
};

export default BlockBody;
