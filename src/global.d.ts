import { compose } from "redux";

type Config = {
  apiBaseURL: string;
  authority: string;
  clientID: string;
  version: string;
  collectorURL?: string;
};

export declare global {
  interface Window {
    APP_CONFIG: Config;
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}
