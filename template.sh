#!/bin/sh

set -eu

SEMVER_REGEX="^(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)(\\-[0-9A-Za-z-]+(\\.[0-9A-Za-z-]+)*)?(\\+[0-9A-Za-z-]+(\\.[0-9A-Za-z-]+)*)?$"

if echo "$VERSION" | grep -qE "$SEMVER_REGEX"; then
  OTEL_VERSION="semver:$VERSION"
else
  OTEL_VERSION="git:$COMMIT"
fi

cat - <<EOF > /usr/share/nginx/html/config.js
window.APP_CONFIG = {
  apiBaseURL: "$API_BASE_URL",
  authority: "$AUTHORITY",
  clientID: "$CLIENT_ID",
  collectorURL: "$COLLECTOR_URL",
  version: "$OTEL_VERSION",
};
EOF
